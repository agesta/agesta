import GitlabCorner from './gitlab-corner';
import Navbar from './navbar';

export default function Layout({ children }) {
    return (
        <div>
            <GitlabCorner />
            {/* <Navbar /> */}
            <div className="container mt-6">{children}</div>
        </div>
    );
}
