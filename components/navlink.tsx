import { useRouter } from 'next/router';
import Link from 'next/link';
import clsx from 'clsx';

export type NavLinkProps = {
    href: string;
    children: React.ReactNode;
};

export default function NavLink({ href, children }: NavLinkProps) {
    const router = useRouter();

    const isActive = router.pathname === href;

    const className = clsx('navbar-item', 'is-tab', isActive && 'is-active');

    return (
        <Link href={href}>
            <a className={className}>{children}</a>
        </Link>
    );
}
