import NavLink from './navlink';

// TODO: mettre la version dans un footer
// const version = process.env.NEXT_PUBLIC_APP_VERSION;

export default function Navbar() {
    return (
        <div>
            <nav className="navbar">
                <div className="navbar-start">
                    <NavLink href="/calculator">Calculette</NavLink>
                    <NavLink href="/whitepaper">White paper</NavLink>
                    <NavLink href="/roadmap">Roadmap</NavLink>
                </div>
            </nav>
        </div>
    );
}
