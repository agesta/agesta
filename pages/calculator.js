import { useForm } from 'react-hook-form';
import { yupResolver } from '@hookform/resolvers/yup';
import * as yup from 'yup';
import clsx from 'clsx';
import { useState } from 'react';
import { explore } from '../libs/reward-optimizer';
import { DateTime } from 'luxon';

const schema = yup.object().shape({
    coins: yup.number().min(0).required(),
    rewards: yup.number().min(0).required(),
    interest: yup.number().min(0).required(),
    fees: yup.number().min(0).required(),
});

export default function Calculator() {
    const { handleSubmit, register, formState } = useForm({
        resolver: yupResolver(schema),
        defaultValues: {
            coins: 100,
            interest: 10,
            rewards: 0,
            fees: 0.005,
        },
    });

    const [result, setResult] = useState(null);

    const onSubmit = ({ coins, interest, fees, rewards }) => {
        const exploreResult = explore({
            coins,
            fees,
            rewards,
            dayInterest: interest / (100 * 365),
        });

        console.log(exploreResult);

        const daysBeforeFirstStack = exploreResult.solution[0];

        if (daysBeforeFirstStack == 0) {
            setResult('maintenant');
        } else {
            const stackTime = DateTime.now()
                .plus({ days: exploreResult.solution[0] })
                .setLocale('fr')
                .toLocaleString(DateTime.DATETIME_MED);

            const totalRewards = rewards + (interest / (100 * 365)) * daysBeforeFirstStack * coins;

            setResult(
                `le ${stackTime} lorsque vos rewards seront proche de ${(+totalRewards.toFixed(5)).toLocaleString()}`
            );
        }
    };

    return (
        <div style={{ maxWidth: '500px', margin: '0 auto' }}>
            <h1 className="title has-text-centered">Optimisation du stacking</h1>
            <form className="container px-3 mt-6" onSubmit={handleSubmit(onSubmit)}>
                <div className="field">
                    <label className="label">Jetons stackés</label>
                    <div className="control ">
                        <input
                            className={clsx('input', formState.errors.coins && 'is-danger')}
                            {...register('coins')}
                        />
                        {formState.errors.coins && (
                            <p className="help is-danger">Le nombre de jetons stackés doit être un nombre positif</p>
                        )}
                    </div>
                </div>
                <div className="field">
                    <label className="label">Rewards</label>
                    <div className="control ">
                        <input
                            className={clsx('input', formState.errors.rewards && 'is-danger')}
                            {...register('rewards')}
                        />
                        {formState.errors.rewards && (
                            <p className="help is-danger">Le nombre de rewards doit être un nombre positif</p>
                        )}
                    </div>
                </div>
                <div className="field">
                    <label className="label">Taux Reward (%)</label>
                    <div className="control">
                        <input
                            className={clsx('input', formState.errors.interest && 'is-danger')}
                            {...register('interest')}
                        />
                        {formState.errors.interest && (
                            <p className="help is-danger">Le taux de reward doit être un nombre positif</p>
                        )}
                    </div>
                </div>
                <div className="field">
                    <label className="label">Fees</label>
                    <div className="control">
                        <input className={clsx('input', formState.errors.fees && 'is-danger')} {...register('fees')} />
                        {formState.errors.fees && <p className="help is-danger">Fees doit être un nombre positif</p>}
                    </div>
                </div>
                <div className="field has-text-centered">
                    <input className="button is-primary" type="submit" value="Calculer" />
                </div>
            </form>
            {result && <div className="notification is-success mt-6">Vous devriez restacker {result}.</div>}
        </div>
    );
}
