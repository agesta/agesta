module.exports = {
    reactStrictMode: true,

    async redirects() {
        return [
            {
                source: '/',
                destination: '/calculator',
                permanent: false,
            },
        ];
    },
};
