type Config = {
    coins: number;
    rewards: number;
    fees: number;
    dayInterest: number;
};

function simulate(config: Config, solution: number[]) {
    let coins = config.coins + config.coins * config.dayInterest * solution[0] + config.rewards - config.fees;
    for (let i = 1; i < solution.length; ++i) {
        coins = coins + coins * config.dayInterest * solution[i] - config.fees;
    }
    return coins;
}

// ex: 4 restack sur 100 jours: [25, 25, 25, 25]
function initSolution(nbRestack: number, nbDays: number): number[] {
    return new Array(nbRestack).fill(nbDays / nbRestack);
}

function prepareNeigbors(length: number): [number, number][] {
    const pairs: [number, number][] = [];
    for (let i = 0; i < length; ++i) {
        for (let j = i + 1; j < length; ++j) {
            pairs.push([i, j]);
            pairs.push([j, i]);
        }
    }
    return pairs;
}

function* neighbors(solution: number[], delta: number, pairs: [number, number][]): Generator<number[]> {
    for (const [a, b] of pairs) {
        if (solution[b] <= delta) {
            delta = solution[b];
        }

        const newSolution = [...solution];
        newSolution[a] += delta;
        newSolution[b] -= delta;
        yield newSolution;
    }
}

function firstImprovement(config: Config, solution: number[], score: number, delta: number, pairs: [number, number][]) {
    for (const neighbor of neighbors(solution, delta, pairs)) {
        const neighborScore = simulate(config, neighbor);
        if (neighborScore > score)
            return {
                solution: neighbor,
                score: neighborScore,
            };
    }
    return null;
}

const MAX_DELTA = 20;
const MIN_DELTA = 0.0001;
function optimize(config: Config, nbRestack: number, nbDays: number) {
    const pairs = prepareNeigbors(nbRestack);
    let solution = initSolution(nbRestack, nbDays);
    let score = simulate(config, solution);
    let iterations = 0;

    let delta = MAX_DELTA;
    while (delta >= MIN_DELTA) {
        iterations++;
        const improvement = firstImprovement(config, solution, score, delta, pairs);

        if (!improvement) {
            delta = delta * 0.5;
        } else {
            solution = improvement.solution;
            score = improvement.score;
        }
    }

    return { solution, score, iterations };
}

const MAX_RESTACK = 55;
const MIN_RESTACK = 35;
function exploreNbRestack(config: Config, nbDays: number) {
    let bestResult = optimize(config, MIN_RESTACK, nbDays);
    let iterations = bestResult.iterations;

    for (let i = MIN_RESTACK + 1; i <= MAX_RESTACK; ++i) {
        const result = optimize(config, i, nbDays);
        iterations += result.iterations;

        if (!bestResult || result.score > bestResult.score) {
            bestResult = result;
        } else {
            break;
        }
    }

    bestResult.iterations = iterations;
    return bestResult;
}

export function explore(config: Config) {
    let nbDays = 1000 // TODO: trouver une formule pour initaliser en fonction de coins et interest
    let iterations = 0;

    while (true) {
        const result = exploreNbRestack(config, nbDays);
        iterations += result.iterations;

        if (result.solution.length === MIN_RESTACK) {
            nbDays = nbDays * 1.3;
        } else if (result.solution.length === MAX_RESTACK) {
            nbDays = nbDays / 1.3;
        } else {
            result.iterations = iterations;
            return result;
        }
    }
}
